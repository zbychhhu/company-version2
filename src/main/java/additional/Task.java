package additional;

public class Task {
    private String task;
    private int unitsOfWork;

    public Task(String task, int unitsOfWork) {
        this.task = task;
        this.unitsOfWork = unitsOfWork;
    }

    public int getUnitsOfWork() {
        return unitsOfWork;
    }

    public String getTask() {
        return task;
    }

    public String toString() {
        return task + ": " + unitsOfWork + "\n";
    }
}
