package employees;

import additional.Role;
import additional.Sex;
import additional.Task;
import report.Report;

public abstract class AbstractEmployee implements Employee {

    private Role role;
    private final String name;
    private final String surname;
    private final String university;
    private final Sex sex;
    private final String nationality;
    private final String email;


    public AbstractEmployee(Builder employee) {
        this.role = employee.role;
        this.name = employee.name;
        this.surname = employee.surname;
        this.university = employee.university;
        this.sex = employee.sex;
        this.nationality = employee.nationality;
        this.email = employee.email;

    }

    @Override
    public abstract void assign(Task task);

    @Override
    public abstract Report reportWork();

    @Override
    public abstract Task getTask();

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getSurname() {
        return surname;
    }

    @Override
    public String getUniversity() {
        return university;
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public String getNationality() {
        return nationality;
    }

    @Override
    public Sex getSex() {
        return sex;
    }

    @Override
    public Role getRole() {
        return role;
    }

    public static abstract class Builder<T> {
        private Role role;
        private final String name;
        private final String surname;
        private final String university;
        private final Sex sex;
        private final String nationality;
        private final String email;


        public Builder(Role role, String name, String surname, String university, Sex sex, String nationality, String email) {
            this.role = role;
            this.name = name;
            this.surname = surname;
            this.university = university;
            this.sex = sex;
            this.nationality = nationality;
            this.email = email;

        }

        public abstract AbstractEmployee buildEmployee() throws Exception;


    }

    @Override
    public String toString() {
        return "Role: " + role + " Name: " + name + " Surname: " + surname + " Univeristy: " + university
                + " Sex: " + sex + " Nationality: " + nationality + " email: " + email;
    }
}
