package employees;

import additional.Role;
import additional.Sex;
import additional.Task;
import report.DeveloperReport;
import report.Report;

import java.util.List;

public class Developer extends AbstractEmployee {

    private DeveloperReport report;
    private Task task;

    public Developer(Builder employee) {

        super(employee);
        report = new DeveloperReport(this);
    }


    @Override
    public Task getTask() {
        return task;
    }

    @Override
    public void assign(Task task) {

        this.task = task;
        report.saveToRaport(task);
    }

    @Override
    public Report reportWork() {

        return report;

    }

    @Override
    public List<Report> reportWork(List<Report> reports) {
        reports.add(report);
        return reports;
    }

    public static class Builder extends AbstractEmployee.Builder<Builder> {
        public Builder(String name, String surname, String university, Sex sex, String nationality, String email) {
            super(Role.Developer, name, surname, university, sex, nationality, email);
        }

        @Override
        public Developer buildEmployee() {
            return new Developer(this);
        }
    }

    @Override
    public String toString() {
        return super.toString() + " Task: " + task.getTask() + " " + reportWork();
    }
}
