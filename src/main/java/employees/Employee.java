package employees;

import additional.Role;
import additional.Sex;
import additional.Task;
import report.Report;

import java.util.List;

public interface Employee {

    String getName();

    String getSurname();

    String getUniversity();

    String getEmail();

    Sex getSex();

    String getNationality();

    Role getRole();

    Task getTask();

    Report reportWork();

    List<Report> reportWork(List<Report> reports);

    void assign(Task task);

}
