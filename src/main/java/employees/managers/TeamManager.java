package employees.managers;

import additional.HireType;
import additional.Role;
import additional.Sex;
import additional.Task;
import employees.AbstractEmployee;
import employees.Employee;
import hiringability.*;
import report.*;

import java.util.ArrayList;
import java.util.List;

public class TeamManager extends AbstractEmployee implements Manager {


    Report report;
    protected List<Employee> listEmployees;
    private final int max = 10;
    private CanHire hire;


    public TeamManager(Builder employee) throws Exception {
        super(employee);
        listEmployees = new ArrayList<>(max);
        report = new TeamManagerReport(this);
        this.hire = typeOfManager(employee.getHireType());


    }

    public CanHire typeOfManager(HireType hireType) throws Exception {

        if (hireType.equals(HireType.AGH))
            return new CanHireAGH();
        else if (hireType.equals(HireType.Polish))
            return new CanHirePolish();
        else if (hireType.equals(HireType.Gmail))
            return new CanHireGmail();
        else if (hireType.equals(HireType.Male))
            return new CanHireMale();
        else if (hireType.equals(HireType.Female))
            return new CanHireFemale();
        else throw new Exception("hireType not defined");
    }


    @Override
    public Report reportWork() {


        return report;

    }

    @Override
    public List<Report> reportWork(List<Report> reports) {

        reports.add(report);
        listEmployees.forEach(employee -> employee.reportWork(reports));

        return reports;
    }


    @Override
    public Task getTask() {
        return null;
    }

    @Override
    public void assign(Task task) {

        listEmployees.forEach(employee -> {
            if (employee.getTask() == null) {
                employee.assign(task);
                report.saveToRaport(task);
            }
        });


    }

    @Override
    public void hire(Employee employee) throws Exception {

        if (hire.canHire(employee)) {
            listEmployees.add(employee);
            if (employee.getTask() != null) {
                report.saveToRaport(employee.getTask());
            }
        } else throw new Exception("TeamManager can't hire worker");

    }

    @Override
    public void fire(Employee e) throws Exception {
        if (listEmployees.size() == 0)
            listEmployees.remove(e);
        else throw new Exception("TeamManager doesn't have a workers");
    }

    @Override
    public boolean canHire() {
        return listEmployees.size() < max;
    }

    public static class Builder extends AbstractEmployee.Builder<Builder> {

        private HireType hireType;


        public Builder(Role role, String name, String surname, String university, Sex sex, String nationality, String email, HireType hireType) {
            super(role, name, surname, university, sex, nationality, email);
            this.hireType = hireType;


        }

        @Override
        public TeamManager buildEmployee() throws Exception {
            return new TeamManager(this);
        }

        public HireType getHireType() {
            return hireType;
        }
    }

    @Override
    public String toString() {

        return super.toString() + " Team: \t\n\t" + listEmployees.toString() + "\t";
    }
}
