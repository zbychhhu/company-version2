package hiringability;

import employees.Employee;

public interface CanHire {

    boolean canHire(Employee employee);
}
