package hiringability;

import employees.Employee;

import java.util.function.Predicate;

public class CanHireAGH implements CanHire {

    @Override
    public boolean canHire(Employee employee) {
        Predicate<String> pred = (s) -> s.equals("AGH");
        return pred.test(employee.getUniversity());
    }
}
