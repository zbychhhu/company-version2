package hiringability;

import additional.Sex;
import employees.Employee;

import java.util.function.Predicate;

public class CanHireFemale implements CanHire {
    public boolean canHire(Employee employee) {

        Predicate<Sex> pred = (sex -> sex.equals(Sex.Female));
        return pred.test(employee.getSex());

    }
}
