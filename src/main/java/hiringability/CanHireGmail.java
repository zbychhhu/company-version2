package hiringability;

import employees.Employee;

import java.util.function.Predicate;

public class CanHireGmail implements CanHire {
    public boolean canHire(Employee employee) {

        Predicate<String> pred = s -> s.contains("gmail.com");
        return pred.test(employee.getEmail());

    }
}
