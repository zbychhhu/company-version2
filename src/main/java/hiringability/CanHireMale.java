package hiringability;

import additional.Sex;
import employees.Employee;

import java.util.function.Predicate;

public class CanHireMale implements CanHire {
    public boolean canHire(Employee employee) {

        Predicate<Sex> pred = (sex -> sex.equals(Sex.Male));
        return pred.test(employee.getSex());

    }

}
