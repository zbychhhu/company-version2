package hiringability;

import employees.Employee;

import java.util.function.Predicate;

public class CanHirePolish implements CanHire {
    public boolean canHire(Employee employee) {

        Predicate<String> pred = s -> s.equals("Polish");
        return pred.test(employee.getNationality());

    }
}
