package random;

import additional.HireType;
import additional.Role;
import additional.Sex;
import additional.Task;
import employees.AbstractEmployee;
import employees.Developer;
import employees.Employee;
import employees.managers.TeamManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

public class RandomStructure {

    private int size;
    private Data data;
    private List<Employee> company;
    private List list;
    private Random random;
    private String[] tasks;

    public RandomStructure(int size, List<String> listNames, List<String> listSurnames, List<String> listDomains) throws Exception {

        data = new Data(listNames, listSurnames, listDomains);
        this.size = size;
        company = new ArrayList<>(10);
        list = new ArrayList(10);
        random = new Random();
        tasks = new String[]{"Implement new function", "Fix bugs", "Refactor", "Get to know new technology"};

        randomMachine();

    }

    private String randomSurname() throws IOException {
        return RandomElement(data.getListSurnames());
    }

    private String randomName() throws IOException {

        return RandomElement(data.getListNames());

    }

    private String randomDomain() throws IOException {

        return RandomElement(data.getListDomain());

    }

    private String RandomElement(List<String> list) {

        return list.get(random.nextInt(list.size()));
    }

    public Developer randomDeveloper() throws IOException {
        String name = randomName();
        String surname = randomSurname();
        String email = name + surname + "@" + randomDomain();

        return new Developer.Builder(name, surname, "AGH", Sex.Male, "Polish", email).buildEmployee();
    }

    public TeamManager randomDevelopmentManager() throws IOException, Exception {
        String name = randomName();
        String surname = randomSurname();
        String email = name + surname + "@gmail.com";

        return new TeamManager.Builder(Role.Development_manager, name, surname, "AGH", Sex.Male, "Polish", email, HireType.Polish).buildEmployee();
    }


    private Task randomTask() {
        return new Task(tasks[random.nextInt(tasks.length)], random.nextInt(50) + 1);
    }

    private void randomCompany() throws Exception {
        company.add(new TeamManager.Builder(Role.CEO, "Boss", "Kowalski", "AGH", Sex.Male, "Polish", "Boss@gmail.com", HireType.AGH).buildEmployee());
        IntStream.range(1, size).forEach(i -> {

            if (i + 1 < size) {
                IntStream.range(0, (int) Math.pow(2, i)).forEach(j -> {
                    try {
                        company.add(randomDevelopmentManager());
                    } catch (Exception e) {
                        System.out.println(e);
                    }
                });
            } else {
                IntStream.range(0, (int) Math.pow(2, i)).forEach(j -> {
                    try {
                        company.add(randomDeveloper());
                    } catch (Exception e) {
                        System.out.println(e);
                    }
                });
            }

        });
    }


    private void randomHire() {

        int j = 0;
        for (int i = 1; i < company.size(); i++) {
            try {
                ((TeamManager) company.get(j)).hire(company.get(i));
            } catch (Exception e) {
                System.out.println(e);
            }
            if (i % 2 == 0)
                j++;

        }


    }

    private void randomAssign() {
        for (Employee e : company) {
            e.assign(randomTask());

        }
    }

    public Employee getCeo() {
        return company.get(0);
    }

    private void randomMachine() throws Exception {
        randomCompany();
        randomHire();
        randomAssign();
    }

    @Override
    public String toString() {

        return company.toString();
    }


}
