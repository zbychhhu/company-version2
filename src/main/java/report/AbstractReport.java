package report;

import additional.Task;
import employees.Employee;

public abstract class AbstractReport implements Report {
    private int sum;

    protected Employee employee;

    public AbstractReport(Employee employee) {
        sum = 0;
        this.employee = employee;
    }

    public int getSum() {
        return sum;
    }

    public void setSum(int sum) {
        this.sum = sum;
    }

    public abstract void saveToRaport(Task task);

    @Override
    public String toString() {
        return "Unit of work: " + String.valueOf(getSum()) +
                " Name: " + employee.getName() +
                " Surname: " + employee.getSurname() +
                " Role: " + employee.getRole();
    }
}
