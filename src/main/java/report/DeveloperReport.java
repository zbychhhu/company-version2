package report;

import additional.Task;
import employees.Employee;

public class DeveloperReport extends AbstractReport {


    public DeveloperReport(Employee employee) {
        super(employee);
    }

    @Override
    public void saveToRaport(Task task) {
        setSum(task.getUnitsOfWork());
    }

    @Override
    public String toString() {
        return super.toString() + " Units of work " + employee.getTask().getUnitsOfWork();
    }
}
