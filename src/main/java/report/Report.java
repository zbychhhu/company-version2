package report;

import additional.Task;

public interface Report {

    int getSum();

    void saveToRaport(Task task);

}
