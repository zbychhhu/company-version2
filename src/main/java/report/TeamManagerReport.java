package report;


import additional.Task;
import employees.Employee;

public class TeamManagerReport extends AbstractReport {

    public TeamManagerReport(Employee employee) {
        super(employee);
    }

    @Override
    public void saveToRaport(Task task) {
        setSum(getSum() + task.getUnitsOfWork());
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
